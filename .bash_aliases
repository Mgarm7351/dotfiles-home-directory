#####
#
#  command aliases
#
#####
alias home='cd ~'
alias lsl='ls -l'
alias lsla='lsl -a'
alias cls='clear'
alias clsl='cls;lsl'
alias clsla='clsl -a'
alias up='cd ..'
alias n19='nice -19 '
alias ht='htop'

#####
#
#  application aliases
#
#####
alias feh='feh --scale-down'
alias manpg='tldr $1'
alias vlc='vlc --width=800 --height=600'
